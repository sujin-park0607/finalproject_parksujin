package com.likelion.finalproject.domain.dto.user;

import com.likelion.finalproject.domain.entity.User;
import com.likelion.finalproject.enums.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "회원 가입 DTO")
public class UserJoinRequest {

    @Schema(description = "아이디", example = "sujin0607")
    private String userName;

    @Schema(description = "비밀번호", example = "123456")
    private String password;

    @Schema(description = "사용자 권한(USER|ADMIN)", defaultValue = "USER", allowableValues = {"USER", "ADMIN"})
    private UserRole role;

    public User toEntity(String password) {
        return User.builder()
                .userName(this.userName)
                .password(password)
                .role(role.USER)
                .build();
    }
}
