package com.likelion.finalproject.controller;

import com.likelion.finalproject.domain.dto.Response;
import com.likelion.finalproject.domain.dto.comment.CommentDeleteResponse;
import com.likelion.finalproject.domain.dto.comment.CommentRequest;
import com.likelion.finalproject.domain.dto.comment.CommentResponse;
import com.likelion.finalproject.service.CommentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name = "Comment", description = "댓글 API")
@RestController
@RequestMapping("/api/v1/posts/{postId}/comments")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    /**
     * 댓글 전체 조회
     */
    @Tag(name = "Comment", description = "댓글 API")
    @ApiOperation(
            value = "댓글 전체 조회"
            , notes = "게시물 ID를 통해 전체 댓글을 조회한다"
    )
    @ApiImplicitParam(
            name = "postId"
            , value = "게시물 아이디"
            , required = true
            , dataType = "Long"
            , paramType = "path"
            , defaultValue = "None")
    @GetMapping
    public Response<Page<CommentResponse>> getCommentList(@PathVariable Long postId){
        PageRequest pageable = PageRequest.of(0,10, Sort.by("id").descending());
        Page<CommentResponse> commentGetResponseList = commentService.getAllComment(postId, pageable);
        return Response.success(commentGetResponseList);
    }


    /**
     * 댓글 작성
     */
    @Tag(name = "Comment", description = "댓글 API")
    @ApiOperation(
            value = "댓글 작성"
            , notes = "게시물 ID, Token을 받아 댓글 작성"
    )
    @ApiImplicitParams(
        {
            @ApiImplicitParam(
                    name = "postId"
                    , value = "게시물 아이디"
                    , required = true
                    , dataType = "Long"
                    , paramType = "path"
                    , defaultValue = "None")
        })
    @PostMapping
    public Response<CommentResponse> writeComment(@PathVariable Long postId, @RequestBody CommentRequest request, Authentication authentication){
        String userName = authentication.getName();
        CommentResponse commentAddResponse = commentService.addComment(postId, request, userName);
        return Response.success(commentAddResponse);
    }

    /**
     * 댓글 수정
     */
    @Tag(name = "Comment", description = "댓글 API")
    @ApiOperation(
            value = "댓글 수정"
            , notes = "게시물 ID, 댓글 ID, token을 받아 댓글 수정"
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "postId"
                            , value = "게시물 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
                    ,
                    @ApiImplicitParam(
                            name = "id"
                            , value = "수정할 댓글 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
            })
    @PutMapping("/{id}")
    public Response<CommentResponse> updateComment(@PathVariable Long postId, @PathVariable Long id, @RequestBody CommentRequest request, Authentication authentication){
        String userName = authentication.getName();
        CommentResponse commentUpdateResponse = commentService.updateComment(postId, id, request, userName);
        return Response.success(commentUpdateResponse);
    }

    /**
     * 댓글 삭제
     */
    @Tag(name = "Comment", description = "댓글 API")
    @ApiOperation(
            value = "댓글 삭제"
            , notes = "게시물 ID, 댓글 ID, token을 받아 댓글 삭제"
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "postId"
                            , value = "삭제할 게시물 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
                    ,
                    @ApiImplicitParam(
                            name = "id"
                            , value = "삭제할 댓글 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
            })
    @DeleteMapping("/{id}")
    public Response<CommentDeleteResponse> DeleteComment(@PathVariable Long postId, @PathVariable Long id, Authentication authentication){
        String userName = authentication.getName();
        CommentDeleteResponse commentDeleteResponse = commentService.deleteComment(postId, id, userName);
        return Response.success(commentDeleteResponse);
    }

}
