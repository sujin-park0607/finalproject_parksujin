package com.likelion.finalproject.controller;

import com.likelion.finalproject.domain.dto.Response;
import com.likelion.finalproject.service.LikeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Like", description = "좋아요 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts/{postId}/likes")
public class LikeController {

    private final LikeService likeService;
    /**
     * 좋아요 및 좋아요 취소
     */
    @Tag(name = "Like", description = "좋아요 API")
    @ApiOperation(
            value = "좋아요, 좋아요 취소"
            , notes = "게시물 ID를 받아 한번 요청을 보내면 좋아요, 두번 보내게 되면 좋아요 취소 구현(권한 필요)"
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "postId"
                            , value = "게시물 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
            })
    @PostMapping
    public Response<String> addLike(@PathVariable Long postId, Authentication authentication){
        String userName = authentication.getName();
        String message = likeService.controlLike(postId, userName);
        return Response.success(message);
    }

    /**
     * 좋아요 개수 조회
     */
    @Tag(name = "Like", description = "좋아요 API")
    @ApiOperation(
            value = "좋아요 개수 조회"
            , notes = "게시물 ID를 받아 좋아요 개수 확인"
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "postId"
                            , value = "게시물 아이디"
                            , required = true
                            , dataType = "Long"
                            , paramType = "path"
                            , defaultValue = "None")
            })
    @GetMapping
    public Response<Integer> getLike(@PathVariable Long postId){
        Integer count = likeService.countLike(postId);
        return Response.success(count);
    }

}
