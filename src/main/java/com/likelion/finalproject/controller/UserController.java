package com.likelion.finalproject.controller;

import com.likelion.finalproject.domain.dto.Response;
import com.likelion.finalproject.domain.dto.user.*;
import com.likelion.finalproject.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@Tag(name = "User", description = "사용자 API")
@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @Tag(name = "User", description = "사용자 API")
    @ApiOperation(
            value = "회원 가입"
            , notes = "회원 가입 성공 시 userID와 userName을 반환"
    )
    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest request){
        UserDto user = userService.join(request);
        return Response.success(new UserJoinResponse(user.getUserId(), user.getUserName()));
    }

    @Tag(name = "User", description = "사용자 API")
    @ApiOperation(
            value = "로그인"
            , notes = "로그인 성공 시 JWT Token 반환"
    )
    @PostMapping("/login")
    public Response<UserLoginResponse> login(@RequestBody UserLoginRequest request){
        String token = userService.login(request.getUserName(), request.getPassword());
        return Response.success(new UserLoginResponse(token));
    }

    @Tag(name = "User", description = "사용자 API")
    @ApiOperation(
            value = "권한 변경"
            , notes = "변경할 사용자의 ID를 받아 사용자 권한 변경 - 권한이 ADMIN인 유저만 가능"
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "id"
                            , value = "변경할 사용자의 ID"
                            , required = true
                            , dataType = "Long")
            })
    @PostMapping("/{id}/role/change")
    public Response<UserRoleResponse> changeRole(@PathVariable Long id, @RequestBody UserRoleRequest request, Authentication authentication){
        String userName = authentication.getName();
        UserRoleResponse response = userService.changeRole(id, request, userName);
        return Response.success(response);
    }

}
