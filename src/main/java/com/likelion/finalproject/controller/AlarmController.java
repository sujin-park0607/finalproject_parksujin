package com.likelion.finalproject.controller;

import com.likelion.finalproject.domain.dto.Response;
import com.likelion.finalproject.domain.dto.alarm.AlarmResponse;
import com.likelion.finalproject.domain.dto.post.PostGetResponse;
import com.likelion.finalproject.service.AlarmService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "Alarm", description = "알람 API")
@RestController
@RequestMapping("/api/v1/alarms")
@RequiredArgsConstructor
public class AlarmController {

    private final AlarmService alarmService;
    /**
     * 알람 기능
     */
    @Tag(name = "Alarm", description = "알람 API")
    @Operation(
            summary = "알람 조회"
            , description = "자신이 올린 포스팅에 대한 댓글, 좋아요 여부에 대한 알람을 조회할 수 있음"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "알람 내역 조회 성공") })
    @GetMapping
    public Response<Page<AlarmResponse>> getAlarms(Authentication authentication){
        PageRequest pageable = PageRequest.of(0,20, Sort.by("id").descending());
        String userName = authentication.getName();
        Page<AlarmResponse> alarmGetResponseList = alarmService.getAlarmList(userName, pageable);
        return Response.success(alarmGetResponseList);
    }

}
